#ifndef FFMPEGWITHNVENC_H
#define FFMPEGWITHNVENC_H

#include <iostream>
#include <fstream>
#include <thread>
#include <mutex>
#include <csignal>
#include <vector>
#include <sstream>
#include <queue>
#include <sys/types.h>
#include <sys/stat.h>
//#include <sys/stat.h>

#include <ctime>

//NVENC
//#include "nvEncodeAPI.h"
#include "GPUEncodingWithLegacyVersion/nvenc_3.0_linux_sdk/Samples/nvEncodeApp/inc/nvEncodeAPI.h"
//#include "../common/inc/nvUtils.h"
#include "NvEncoder.h"
//#include "../common/inc/nvFileIO.h"
#define BITSTREAM_BUFFER_SIZE 2 * 1024 * 1024

/*Need to explicit tell the compiler that these includes are from C-Language*/
extern "C" {
  //#include <stdint.h>
    #include <sys/time.h>
    #include <libavcodec/avcodec.h>
    #include <libavformat/avformat.h>
    #include <libavutil/imgutils.h>
    #include <libswscale/swscale.h>
    #include <libavutil/opt.h>
    //#include "nvUtils.h"
}


#define SOURCE_VIDEO_HEIGHT 1680
#define SOURCE_VIDEO_WIDTH 4096
#define TOTAL_RES 5

//PARALLEL for Paralization encoding
//SEQUENTIAL for sequantial encoding, TOTAL_THREADS < 2 is squential
//THREADING for encoding with using thread pool
#define THREADING
//Test on 16 threads
#define TOTAL_THREADS 64
#define GPU_THREADS 2
#define THREADS 1
#define SOURCE_FOLDER "Video/"
#define TILES2X2 "2X2"
#define TILES4X4 "4X4"
#define TILES8X8 "8X8"

//Choose which option under
#define T4


#define DEBUG

#define CPUGPU
#if defined (GPU)
    #define ENCODER "nvenc"
    #define PXLFORMAT AV_PIX_FMT_YUV420P
	//#define PXLFORMAT AV_PIX_FMT_YUV444P
	//#define PXLFORMAT AV_PIX_FMT_NV12
#elif defined (CPU)
    #define ENCODER "libx264"
   #define PXLFORMAT AV_PIX_FMT_YUV422P
	//#define PXLFORMAT AV_PIX_FMT_NV12
	//#define PXLFORMAT AV_PIX_FMT_YUV420P
#elif defined (CPUGPU)
	#define ENCODER "libx264"
    //#define PXLFORMAT AV_PIX_FMT_YUV422P
	#define PXLFORMAT AV_PIX_FMT_YUV420P
	//#define PXLFORMAT AV_PIX_FMT_NV12
#endif // defined
//SWS_FAST_BILINEAR
//SWS_BICUBIC
//SWS_BILINEAR 
#define SWS_PARAMETER SWS_FAST_BILINEAR

#if defined (T1)
    #define TILEWIDTH 4096
    #define TILEHEIGHT 1680
    #define XTILES 1
    #define YTILES 1
    #define TILES 1
	#define GPUHEIGHT 1680
#elif defined (T2)
    #define TILEWIDTH 2048
    #define TILEHEIGHT 840
    #define XTILES 2
    #define YTILES 2
    #define TILES 4
	#define GPUHEIGHT 840
#elif defined (T3)
    #define TILEWIDTH 1024
    #define TILEHEIGHT 420
    #define XTILES 4
    #define YTILES 4
    #define TILES 16
	#define GPUHEIGHT 420
#elif defined (T4)
    #define TILEWIDTH 512
    #define TILEHEIGHT 210
    #define XTILES 8
    #define YTILES 8
    #define TILES 64
	#define GPUHEIGHT 216
#elif defined (T5)
	#define TILEWIDTH 256
    #define TILEHEIGHT 120
    #define XTILES 16
    #define YTILES 14
    #define TILES 224
#endif // defined




struct Task {
    AVCodecContext *cContext;
    int tileNumber;
    int quality;
    //int frameNum;
    int x, y;
    std::string filename;
};

struct FolderStructure {
    std::string tiles2x2;
    std::string tiles4x4;
    std::string tiles8x8;
    std::string tiles16x16;
    std::string notile;
    std::string test;

    std::string Tile2X2_crf_folders[TOTAL_RES];
    std::string Tile4X4_crf_folders[TOTAL_RES];
    std::string Tile8X8_crf_folders[TOTAL_RES];
    std::string NoTile_crf_folders[TOTAL_RES];
}fStruct;

/*Static Values*/
const std::string crf_values[] = {"21", "24", "30", "36", "48"};
int crf_integers[] = { 21, 24, 30, 36, 48};

/*Global Parameters*/
#define MAX_THREADS 64
/*
int TILES = 0;
int TILEHEIGHT = 0;
int TILEWIDTH = 0;
int XTILES = 0;
int YTILES = 0;
*/
AVFormatContext *inFormatContext = NULL;
int frames_total = 0;
AVCodec *sharedCodec = NULL;
std::queue<Task*> jobs;
std::thread pool[TOTAL_THREADS-1];
std::thread parallel[MAX_THREADS-1];
//struct SwsContext *sws_ctx = NULL;
AVFrame *dummy;



/*Shared Resources*/
std::mutex lockQueue;
AVFrame* buffer[90];


#endif // FFMPEGWITHNVENC_H
