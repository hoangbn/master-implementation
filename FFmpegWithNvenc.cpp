#include "FFmpegWithNvenc.h"
#include "nvUtils.h"
#include "nvFileIO.h"
#include "SigSevHandler.hpp"

AVFrame *getFrame(int index);
AVFrame *getDummyFrame(bool aTile, AVPixelFormat pixel);
Task *getTask();
SwsContext *getSwsContext();

/*Translates the NVENCSTATUS to a string*/
static const char *_nvencGetErrorEnum(NVENCSTATUS error)
{
    switch (error)
    {
        case NV_ENC_SUCCESS:
            return "NVENC Success";

        case NV_ENC_ERR_NO_ENCODE_DEVICE:
            return "NVENC No Available Encoding Device";

        case NV_ENC_ERR_UNSUPPORTED_DEVICE:
            return "NVENC that devices pass by the client is not supported";

        case NV_ENC_ERR_INVALID_ENCODERDEVICE:
            return "NVENC this indicates that the encoder device supplied by the client is not valid";

        case NV_ENC_ERR_INVALID_DEVICE:
            return "NVENC this indicates that device passed to the API call is invalid";

        case NV_ENC_ERR_DEVICE_NOT_EXIST:
            return "NVENC This indicates that device passed to the API call is no longer available and needs to be reinitialized.";

        case NV_ENC_ERR_INVALID_PTR:
            return "NVENC one or more of the pointers passed to the API call is invalid.";

        case NV_ENC_ERR_INVALID_EVENT:
            return "NVENC indicates that completion event passed in ::NvEncEncodePicture() call is invalid.";

        case NV_ENC_ERR_INVALID_PARAM:
            return "NVENC indicates that one or more of the parameter passed to the API call is invalid.";

        case NV_ENC_ERR_INVALID_CALL:
            return "NVENC indicates that an API call was made in wrong sequence/order.";

        case NV_ENC_ERR_OUT_OF_MEMORY:
            return "NVENC indicates that the API call failed because it was unable to allocate enough memory to perform the requested operation.";

        case NV_ENC_ERR_ENCODER_NOT_INITIALIZED:
            return "NVENC indicates that the encoder has not been initialized with::NvEncInitializeEncoder() or that initialization has failed.";

        case NV_ENC_ERR_UNSUPPORTED_PARAM:
            return "NVENC  that an unsupported parameter was passed by the client.";

        case NV_ENC_ERR_LOCK_BUSY:
            return "NVENC indicates that the ::NvEncLockBitstream() failed to lock the output  buffer.";

        case NV_ENC_ERR_NOT_ENOUGH_BUFFER:
            return "NVENC indicates that the size of the user buffer passed by the client is insufficient for the requested operation";

        case NV_ENC_ERR_INVALID_VERSION:
            return "NVENC indicates that an invalid struct version was used by the client";

        case NV_ENC_ERR_MAP_FAILED:
            return "NVENC NvEncMapInputResource() API failed to map the client provided input resource.";

        case NV_ENC_ERR_NEED_MORE_INPUT:
            return "NVENC HW encode driver requires more input buffers to produce an output bitstream";

        case NV_ENC_ERR_ENCODER_BUSY:
            return "NVENC HW encoder is busy encoding and is unable to encode the input. The client should call ::NvEncEncodePicture() again after few milliseconds.";

        case NV_ENC_ERR_EVENT_NOT_REGISTERD:
            return "NVENC completion event passed in ::NvEncEncodePicture() API has not been registered with encoder driver using ::NvEncRegisterAsyncEvent()";

        case NV_ENC_ERR_GENERIC:
            return "NVENC unknown internal error";

        case NV_ENC_ERR_INCOMPATIBLE_CLIENT_KEY:
            return "NVENC Feature not available for current license key type";

        case NV_ENC_ERR_UNIMPLEMENTED:
            return "NVENC Feature has not been implemented yet.";

        case NV_ENC_ERR_RESOURCE_REGISTER_FAILED:
            return "NVENC NvEncRegisterResource failed to register resource.";

        case NV_ENC_ERR_RESOURCE_NOT_REGISTERED:
            return "NVENC Client is attempting to unregister resource that hasn't been registered.";

        case NV_ENC_ERR_RESOURCE_NOT_MAPPED:
            return "NVENC Client is attempting to unmap resource that hasn't been mapped.";
    }

    return "<unknown>";
}

void freeFrame(AVFrame *frame);
/**

        NVENC Sample with some modification

*/

/*Function to convert YUV420 to NV12*/
void convertYUVpitchtoNV12( unsigned char *yuv_luma, unsigned char *yuv_cb, unsigned char *yuv_cr,
                            unsigned char *nv12_luma, unsigned char *nv12_chroma,
                            int width, int height , int srcStride, int dstStride)
{
	
    int y;
    int x;
    if (srcStride == 0)
        srcStride = width;
    if (dstStride == 0)
        dstStride = width;

    for ( y = 0 ; y < height ; y++)
    {
        memcpy( nv12_luma + (dstStride*y), yuv_luma + (srcStride*y) , width );
    }

    for ( y = 0 ; y < height/2; y++)
    {
        for ( x= 0 ; x < width; x=x+2)
        {
            nv12_chroma[(y*dstStride) + x] =    yuv_cb[((srcStride/2)*y) + (x >>1)];
            nv12_chroma[(y*dstStride) +(x+1)] = yuv_cr[((srcStride/2)*y) + (x >>1)];
        }
    }
}

/*Test functions*/
void convertYUVpitchtoNV12tiled16x16(unsigned char *yuv_luma, unsigned char *yuv_cb, unsigned char *yuv_cr,
                                            unsigned char *tiled_luma, unsigned char *tiled_chroma,
                                            int width, int height, int srcStride, int dstStride)
{
    int tileNb, offs;
    int x,y;

    if (srcStride<0) srcStride = width;

    if (dstStride<0) dstStride = width;

    for (y = 0 ; y < height ; y++)
    {
        for (x= 0 ; x < width; x++)
        {
            tileNb = x/16 + (y/16) * dstStride/16;

            offs = tileNb * 256;
            offs += (y % 16) * 16 + (x % 16);
            tiled_luma[offs]   =  yuv_luma[(srcStride*y) + x];
        }
    }

    for (y = 0 ; y < height/2 ; y++)
    {
        for (x= 0 ; x < width; x = x+2)
        {
            tileNb = x/16 + (y/16) * dstStride/16;

            offs = tileNb * 256;
            offs += (y % 16) * 16 + (x % 16);
            tiled_chroma[offs]   =  yuv_cb[(srcStride/2)*y + x/2];
            tiled_chroma[offs+1] =  yuv_cr[(srcStride/2)*y + x/2];
        }
    }
}


/*Test function, tries to convert YUV422 to NV12.
We cannot directly convert YUV422 to NV12 since 422 is down-sampled both vertically and horizontally
Convert YUV422 to YUV444 then to NV12 is one approach which could be implemented
*/
void convertYUV422pitchtoNV12( unsigned char *yuv_luma, unsigned char *yuv_cb, unsigned char *yuv_cr,
                            unsigned char *nv12_luma, unsigned char *nv12_chroma,
                            int width, int height , int srcStride, int dstStride)
{
	int y;
    int x;
	int offset = 0;
    if (srcStride == 0)
        srcStride = width;
    if (dstStride == 0)
        dstStride = width;

    for ( y = 0 ; y < height ; y++)
    {
        memcpy( nv12_luma + (dstStride*y), yuv_luma + (srcStride*y) , width );
    }

    for ( y = 0 ; y < height/2 ; y++)
    {
        for ( x= 0 ; x < width; x=x+2)
        {
            nv12_chroma[(y*dstStride) + x+1] =    yuv_cb[offset];
            nv12_chroma[(y*dstStride) +(x)] = yuv_cr[offset];
			offset++;
        }
    }
	
}
	
/*Not currently used, seems like it is NVENC can use YUV 444 too*/
void convertYUVpitchtoYUV444(unsigned char *yuv_luma, unsigned char *yuv_cb, unsigned char *yuv_cr,
    unsigned char *surf_luma, unsigned char *surf_cb, unsigned char *surf_cr, int width, int height, int srcStride, int dstStride)
{
    int h;

    for (h = 0; h < height; h++)
    {
        memcpy(surf_luma + dstStride * h, yuv_luma + srcStride * h, width);
        memcpy(surf_cb + dstStride * h, yuv_cb + srcStride * h, width);
        memcpy(surf_cr + dstStride * h, yuv_cr + srcStride * h, width);
    }
}

/*- Initialize -
From sample program
*/
CNvEncoder::CNvEncoder()
{
    m_pNvHWEncoder = new CNvHWEncoder;
    m_pDevice = NULL;
    m_cuContext = NULL;
    m_uEncodeBufferCount = 0;
	convertT1 = 0;
	convertT2 = 0;
	
    memset(&m_stEncoderInput, 0, sizeof(m_stEncoderInput));
    memset(&m_stEOSOutputBfr, 0, sizeof(m_stEOSOutputBfr));
    memset(&m_stEncodeBuffer, 0, sizeof(m_stEncodeBuffer));
}

CNvEncoder::~CNvEncoder()
{
	
    if (m_pNvHWEncoder)
    {
        delete m_pNvHWEncoder;
        m_pNvHWEncoder = NULL;
    }
	
}

/*Testing purposes*/
const int Encoders_per_CContext = 16;
CUcontext cudaDevices[1];

static NV_ENCODE_API_FUNCTION_LIST * nvencApi = NULL;
static CUcontext m_cudaContext = NULL;
void Inittest()
{ 
	if(!m_cudaContext){
		 CUdevice cuDevice = 0;
		 cuInit(0);
		 cuDeviceGet(&cuDevice, 0);
		 for(int i = 0; i < 1; i++){
			 cuCtxCreate(&m_cudaContext, 0, cuDevice) ;
			 cuCtxPopCurrent(&m_cudaContext) ;
		 }
	}
	if(!nvencApi){
		 nvencApi = new NV_ENCODE_API_FUNCTION_LIST;
		 memset(nvencApi, 0, sizeof(NV_ENCODE_API_FUNCTION_LIST));
		 nvencApi->version = NV_ENCODE_API_FUNCTION_LIST_VER;
		 NvEncodeAPICreateInstance(nvencApi);
	}
}

/*Initialize a CUDA Context, implementation from sample program*/
NVENCSTATUS CNvEncoder::InitCuda(uint32_t deviceID)
{
	//m_pDevice = cudaDevices[0];
//	return NV_ENC_SUCCESS;
	if(m_cudaContext != NULL) {
		m_pDevice = m_cudaContext;
		return NV_ENC_SUCCESS;
	}
    CUresult cuResult;
    CUdevice device;
    CUcontext cuContextCurr;
    int  deviceCount = 0;
    int  SMminor = 0, SMmajor = 0;

    cuResult = cuInit(0);
    if (cuResult != CUDA_SUCCESS)
    {
        PRINTERR("cuInit error:0x%x\n", cuResult);
        assert(0);
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }

    cuResult = cuDeviceGetCount(&deviceCount);
    if (cuResult != CUDA_SUCCESS)
    {
        PRINTERR("cuDeviceGetCount error:0x%x\n", cuResult);
        assert(0);
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }

    // If dev is negative value, we clamp to 0
    if ((int)deviceID < 0)
        deviceID = 0;

    if (deviceID >(unsigned int)deviceCount - 1)
    {
        PRINTERR("Invalid Device Id = %d\n", deviceID);
        return NV_ENC_ERR_INVALID_ENCODERDEVICE;
    }

    cuResult = cuDeviceGet(&device, deviceID);
    if (cuResult != CUDA_SUCCESS)
    {
        PRINTERR("cuDeviceGet error:0x%x\n", cuResult);
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }

    cuResult = cuDeviceComputeCapability(&SMmajor, &SMminor, deviceID);
    if (cuResult != CUDA_SUCCESS)
    {
        PRINTERR("cuDeviceComputeCapability error:0x%x\n", cuResult);
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }

    if (((SMmajor << 4) + SMminor) < 0x30)
    {
        PRINTERR("GPU %d does not have NVENC capabilities exiting\n", deviceID);
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }

    cuResult = cuCtxCreate((CUcontext*)(&m_pDevice), 0, device);
    if (cuResult != CUDA_SUCCESS)
    {
        PRINTERR("cuCtxCreate error:0x%x\n", cuResult);
        assert(0);
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }
	
    cuResult = cuCtxPopCurrent(&cuContextCurr);
    if (cuResult != CUDA_SUCCESS)
    {
        PRINTERR("cuCtxPopCurrent error:0x%x\n", cuResult);
        assert(0);
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }
    return NV_ENC_SUCCESS;
	
}


//Initialize the CudaContext and set it to global
/*
void *GlobalDevice;
NVENCSTATUS CNvEncoder::InitCuda(uint32_t deviceID)
{
    CUresult cuResult;
    CUdevice device;
    CUcontext cuContextCurr;
    int  deviceCount = 0;
    int  SMminor = 0, SMmajor = 0;

    cuResult = cuInit(0);
    if (cuResult != CUDA_SUCCESS)
    {
        PRINTERR("cuInit error:0x%x\n", cuResult);
        assert(0);
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }

    cuResult = cuDeviceGetCount(&deviceCount);
    if (cuResult != CUDA_SUCCESS)
    {
        PRINTERR("cuDeviceGetCount error:0x%x\n", cuResult);
        assert(0);
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }

    // If dev is negative value, we clamp to 0
    if ((int)deviceID < 0)
        deviceID = 0;

    if (deviceID >(unsigned int)deviceCount - 1)
    {
        PRINTERR("Invalid Device Id = %d\n", deviceID);
        return NV_ENC_ERR_INVALID_ENCODERDEVICE;
    }

    cuResult = cuDeviceGet(&device, deviceID);
    if (cuResult != CUDA_SUCCESS)
    {
        PRINTERR("cuDeviceGet error:0x%x\n", cuResult);
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }

    cuResult = cuDeviceComputeCapability(&SMmajor, &SMminor, deviceID);
    if (cuResult != CUDA_SUCCESS)
    {
        PRINTERR("cuDeviceComputeCapability error:0x%x\n", cuResult);
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }

    if (((SMmajor << 4) + SMminor) < 0x30)
    {
        PRINTERR("GPU %d does not have NVENC capabilities exiting\n", deviceID);
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }

    cuResult = cuCtxCreate((CUcontext*)(&m_pDevice), 0, device);
    if (cuResult != CUDA_SUCCESS)
    {
        PRINTERR("cuCtxCreate error:0x%x\n", cuResult);
        assert(0);
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }

    cuResult = cuCtxPopCurrent(&cuContextCurr);
    if (cuResult != CUDA_SUCCESS)
    {
        PRINTERR("cuCtxPopCurrent error:0x%x\n", cuResult);
        assert(0);
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }
    return NV_ENC_SUCCESS;
}

*/
NVENCSTATUS CNvEncoder::AllocateIOBuffers(uint32_t uInputWidth, uint32_t uInputHeight, uint32_t isYuv444)
{
    NVENCSTATUS nvStatus = NV_ENC_SUCCESS;
	
    m_EncodeBufferQueue.Initialize(m_stEncodeBuffer, m_uEncodeBufferCount);
    for (uint32_t i = 0; i < m_uEncodeBufferCount; i++)
    {
        nvStatus = m_pNvHWEncoder->NvEncCreateInputBuffer(uInputWidth, uInputHeight, &m_stEncodeBuffer[i].stInputBfr.hInputSurface, isYuv444);
        if (nvStatus != NV_ENC_SUCCESS)
            return nvStatus;
		
		
        m_stEncodeBuffer[i].stInputBfr.bufferFmt = isYuv444 ? NV_ENC_BUFFER_FORMAT_YUV444_PL : NV_ENC_BUFFER_FORMAT_NV12_PL;
        m_stEncodeBuffer[i].stInputBfr.dwWidth = uInputWidth;
        m_stEncodeBuffer[i].stInputBfr.dwHeight = uInputHeight;

        nvStatus = m_pNvHWEncoder->NvEncCreateBitstreamBuffer(BITSTREAM_BUFFER_SIZE, &m_stEncodeBuffer[i].stOutputBfr.hBitstreamBuffer);
        if (nvStatus != NV_ENC_SUCCESS)
			assert(0);
          //  return nvStatus;
		
        m_stEncodeBuffer[i].stOutputBfr.dwBitstreamBufferSize = BITSTREAM_BUFFER_SIZE;
        m_stEncodeBuffer[i].stOutputBfr.hOutputEvent = NULL;
    }

    m_stEOSOutputBfr.bEOSFlag = TRUE;
    m_stEOSOutputBfr.hOutputEvent = NULL;

    return NV_ENC_SUCCESS;
}

NVENCSTATUS CNvEncoder::ReleaseIOBuffers()
{
    for (uint32_t i = 0; i < m_uEncodeBufferCount; i++)
    {
        m_pNvHWEncoder->NvEncDestroyInputBuffer(m_stEncodeBuffer[i].stInputBfr.hInputSurface);
        m_stEncodeBuffer[i].stInputBfr.hInputSurface = NULL;

        m_pNvHWEncoder->NvEncDestroyBitstreamBuffer(m_stEncodeBuffer[i].stOutputBfr.hBitstreamBuffer);
        m_stEncodeBuffer[i].stOutputBfr.hBitstreamBuffer = NULL;
    }

    return NV_ENC_SUCCESS;
}

NVENCSTATUS CNvEncoder::FlushEncoder()
{
    NVENCSTATUS nvStatus = m_pNvHWEncoder->NvEncFlushEncoderQueue(m_stEOSOutputBfr.hOutputEvent);
    if (nvStatus != NV_ENC_SUCCESS)
    {
        assert(0);
        return nvStatus;
    }

    EncodeBuffer *pEncodeBufer = m_EncodeBufferQueue.GetPending();
    while (pEncodeBufer)
    {
        m_pNvHWEncoder->ProcessOutput(pEncodeBufer);
        pEncodeBufer = m_EncodeBufferQueue.GetPending();
    }

    return nvStatus;
}

NVENCSTATUS CNvEncoder::Deinitialize(uint32_t devicetype)
{
    NVENCSTATUS nvStatus = NV_ENC_SUCCESS;

    ReleaseIOBuffers();

    nvStatus = m_pNvHWEncoder->NvEncDestroyEncoder();
	
    if (m_pDevice)
    {
		/*
        switch (devicetype)
        {
        case NV_ENC_CUDA:
            CUresult cuResult = CUDA_SUCCESS;
            cuResult = cuCtxDestroy((CUcontext)m_pDevice);
            if (cuResult != CUDA_SUCCESS)
                PRINTERR("cuCtxDestroy error:0x%x\n", cuResult);
        }
*/
        m_pDevice = NULL;
    }

    return nvStatus;
}

/*****
    Load from avframe, use NV12, no need to convert then?


*////

/*
int counter = 0;

AVFrame *temp2 = getDummyFrame(true, AV_PIX_FMT_YUV420P);
NVENCSTATUS loadframe(uint8_t *yuvInput[3], HANDLE hInputYUVFile, uint32_t frmIdx, uint32_t width, uint32_t height, uint32_t &numBytesRead, uint32_t isYuv444)
{
    uint64_t fileOffset;
    uint32_t result;


    //Set size depending on whether it is YUV 444 or YUV 420
    uint32_t dwInFrameSize = isYuv444 ? width * height * 3 : width*height + (width*height) / 2;
    fileOffset = (uint64_t)(dwInFrameSize *frmIdx);
    result = nvSetFilePointer64(hInputYUVFile, fileOffset, NULL, FILE_BEGIN);
    if (result == (uint32_t)INVALID_SET_FILE_POINTER)
    {
        printf("HWY????\n");
        return NV_ENC_ERR_INVALID_PARAM;
    }
    if (isYuv444)
    {
        nvReadFile(hInputYUVFile, yuvInput[0], width * height, &numBytesRead, NULL);
        nvReadFile(hInputYUVFile, yuvInput[1], width * height, &numBytesRead, NULL);
        nvReadFile(hInputYUVFile, yuvInput[2], width * height, &numBytesRead, NULL);
    }
    else
    {
      //  printf("here");

          //   memcpy(yuvInput[0], (void*)&buffer[counter]->data[0], buffer[counter]->linesize[0]*buffer[counter]->height);
       if(counter == 90){
        numBytesRead = 0;
        return NV_ENC_SUCCESS;
       }
       sws_scale(sws_ctx,(uint8_t const * const *)buffer[counter]->data, buffer[counter]->linesize,0 ,SOURCE_VIDEO_HEIGHT ,temp2->data, temp2->linesize);

        memcpy(yuvInput[0], (uint8_t*) temp2->data[0], width * height);
        numBytesRead = width * height;
      //  memcpy(yuvInput[1], (void*)&buffer[counter]->data[1], buffer[counter]->linesize[1]*buffer[counter]->height);
        memcpy(yuvInput[1], (uint8_t*) temp2->data[1], width * height / 4);
        numBytesRead = width * height / 4;
      //  yuvInput[1] = (uint8_t*)&temp2->data[1];
      //  memcpy(yuvInput[2], (void*)&buffer[counter]->data[2], buffer[counter]->linesize[2]*buffer[counter]->height);
        memcpy(yuvInput[2], (uint8_t*) temp2->data[2], width * height / 4);
         //yuvInput[2] = (uint8_t*)&temp2->data[2];
         numBytesRead = width * height / 4;
        counter++;


     //   nvReadFile(hInputYUVFile, yuvInput[0], width * height, &numBytesRead, NULL);
     //   nvReadFile(hInputYUVFile, yuvInput[1], width * height / 4, &numBytesRead, NULL);
    //    nvReadFile(hInputYUVFile, yuvInput[2], width * height / 4, &numBytesRead, NULL);

        //printf("\rTotal data: %d", width*height + width*height/4 +width*height/4);
    }
  //  freeFrame(temp);
  //  freeFrame(temp2);

    return NV_ENC_SUCCESS;
}
*/
void PrintHelp()
{
    printf("Usage : NvEncoder \n"
        "-i <string>                  Specify input yuv420 file\n"
        "-o <string>                  Specify output bitstream file\n"
        "-size <int int>              Specify input resolution <width height>\n"
        "\n### Optional parameters ###\n"
        "-codec <integer>             Specify the codec \n"
        "                                 0: H264\n"
        "                                 1: HEVC\n"
        "-preset <string>             Specify the preset for encoder settings\n"
        "                                 hq : nvenc HQ \n"
        "                                 hp : nvenc HP \n"
        "                                 lowLatencyHP : nvenc low latency HP \n"
        "                                 lowLatencyHQ : nvenc low latency HQ \n"
        "                                 lossless : nvenc Lossless Default \n"
        "-startf <integer>            Specify start index for encoding. Default is 0\n"
        "-endf <integer>              Specify end index for encoding. Default is end of file\n"
        "-fps <integer>               Specify encoding frame rate\n"
        "-goplength <integer>         Specify gop length\n"
        "-numB <integer>              Specify number of B frames\n"
        "-bitrate <integer>           Specify the encoding average bitrate\n"
        "-vbvMaxBitrate <integer>     Specify the vbv max bitrate\n"
        "-vbvSize <integer>           Specify the encoding vbv/hrd buffer size\n"
        "-rcmode <integer>            Specify the rate control mode\n"
        "                                 0:  Constant QP\n"
        "                                 1:  Single pass VBR\n"
        "                                 2:  Single pass CBR\n"
        "                                 4:  Single pass VBR minQP\n"
        "                                 8:  Two pass frame quality\n"
        "                                 16: Two pass frame size cap\n"
        "                                 32: Two pass VBR\n"
        "-qp <integer>                Specify qp for Constant QP mode\n"
        "-picStruct <integer>         Specify the picture structure\n"
        "                                 1:  Progressive frame\n"
        "                                 2:  Field encoding top field first\n"
        "                                 3:  Field encoding bottom field first\n"
        "-devicetype <integer>        Specify devicetype used for encoding\n"
        "                                 0:  DX9\n"
        "                                 1:  DX11\n"
        "                                 2:  Cuda\n"
        "                                 3:  DX10\n"
        "-yuv444 <integer>             Specify the input YUV format\n"
        "                                 0: YUV 420\n"
        "                                 1: YUV 444\n"
        "-deviceID <integer>           Specify the GPU device on which encoding will take place\n"
        "-help                         Prints Help Information\n\n"
        );
}

/*
Take from sample program and modified it
*/
int cudaContext;
NVENCSTATUS CNvEncoder::MyEncodeFrame(AVFrame* frame, bool bFlush, bool newFile)
{
	NVENCSTATUS nvStatus = NV_ENC_SUCCESS;
    uint32_t lockedPitch = 0;
    EncodeBuffer *pEncodeBuffer = NULL;

    if (bFlush)
    {
        FlushEncoder();
        return NV_ENC_SUCCESS;
    }

    if (!frame)
    {
        return NV_ENC_ERR_INVALID_PARAM;
    }

    pEncodeBuffer = m_EncodeBufferQueue.GetAvailable();
    if(!pEncodeBuffer)
    {
        m_pNvHWEncoder->ProcessOutput(m_EncodeBufferQueue.GetPending());
        pEncodeBuffer = m_EncodeBufferQueue.GetAvailable();
    }

    unsigned char *pInputSurface;

    nvStatus = m_pNvHWEncoder->NvEncLockInputBuffer(pEncodeBuffer->stInputBfr.hInputSurface, (void**)&pInputSurface, &lockedPitch);
    if (nvStatus != NV_ENC_SUCCESS)
        return nvStatus;

    
	if(PXLFORMAT == AV_PIX_FMT_YUV420P){
		unsigned char *pInputSurfaceCh = pInputSurface + (pEncodeBuffer->stInputBfr.dwHeight*lockedPitch);
		unsigned long long lStart, lEnd, lFreq;
		NvQueryPerformanceCounter(&lStart);
	
	//printf("Time it took to convert %6.2fms\n", (elapsedTime*1000.0)/lFreq);
		convertYUVpitchtoNV12(frame->data[0], frame->data[1], frame->data[2], pInputSurface, pInputSurfaceCh, frame->linesize[0], frame->height, frame->linesize[0], lockedPitch);
		//convertYUVpitchtoNV12tiled16x16(frame->data[0], frame->data[1], frame->data[2], pInputSurface, pInputSurfaceCh, frame->linesize[0], frame->height, frame->linesize[0], lockedPitch);
		NvQueryPerformanceCounter(&lEnd);
		NvQueryPerformanceFrequency(&lFreq);
		double elapsedTime = (double)(lEnd - lStart);
		convertT2 += elapsedTime;
	} else {
		unsigned char *pInputSurfaceCh = pInputSurface + (pEncodeBuffer->stInputBfr.dwHeight*lockedPitch);
		memcpy(pInputSurface, frame->data[0], frame->linesize[0] * frame->height);		
		memcpy(pInputSurfaceCh, frame->data[1], frame->linesize[1] * frame->height/2);
		//unsigned char *pInputSurfaceCb = pInputSurface + (pEncodeBuffer->stInputBfr.dwHeight * lockedPitch);
     //   unsigned char *pInputSurfaceCr = pInputSurfaceCb + (pEncodeBuffer->stInputBfr.dwHeight * lockedPitch);
     //   convertYUVpitchtoYUV444(frame->data[0], frame->data[1], frame->data[2], pInputSurface, pInputSurfaceCb, pInputSurfaceCr, frame->linesize[0], frame->height, frame->linesize[0], lockedPitch);
	}
  
    nvStatus = m_pNvHWEncoder->NvEncUnlockInputBuffer(pEncodeBuffer->stInputBfr.hInputSurface);
    if (nvStatus != NV_ENC_SUCCESS)
        return nvStatus;
	
	if(newFile){
		NvEncPictureCommand cmd;
		cmd.bForceIDR = true;
		//cmd.bForceIntraRefresh = true;
		nvStatus = m_pNvHWEncoder->NvEncEncodeFrame(pEncodeBuffer, &cmd, TILEWIDTH, TILEHEIGHT, (NV_ENC_PIC_STRUCT)m_uPicStruct);
	} else 
	 nvStatus = m_pNvHWEncoder->NvEncEncodeFrame(pEncodeBuffer, NULL, TILEWIDTH, TILEHEIGHT, (NV_ENC_PIC_STRUCT)m_uPicStruct);
	
    return nvStatus;
	
	
	
}


/*

*/
int CNvEncoder::MyEncodeMain(char * filename, int quality, int tileNumber, bool alive)
{
	//int numFramesEncoded = 0;
    NVENCSTATUS nvStatus = NV_ENC_SUCCESS;
    bool bError = false;
    EncodeConfig encodeConfig;
	
    memset(&encodeConfig, 0, sizeof(EncodeConfig));
	
//	Task *currentTask = getTask();
	
    // std::string str[] = {"-o", "test5.h264", "-size", "4096", "1680", "-qp", "21",  "-rcmode" , "0" , "-goplength", "90", "-numB", "3"};
    encodeConfig.outputFileName = filename;
    encodeConfig.endFrameIdx = INT_MAX;
   // encodeConfig.bitrate = 5000000;
   // encodeConfig.rcMode = NV_ENC_PARAMS_RC_CONSTQP;
    encodeConfig.rcMode = NV_ENC_PARAMS_RC_CONSTQP;
    encodeConfig.gopLength = 90;
    encodeConfig.deviceType = NV_ENC_CUDA;
    encodeConfig.codec = NV_ENC_H264;
    encodeConfig.fps = 30;
    encodeConfig.qp =  atoi(crf_values[quality].c_str());
	// This is the default low latency preset and the quality
	//	and speed is midway of the two other presets. 
    //encodeConfig.presetGUID = NV_ENC_PRESET_DEFAULT_GUID;
	encodeConfig.presetGUID = NV_ENC_PRESET_LOW_LATENCY_HP_GUID;
  
    encodeConfig.pictureStruct = NV_ENC_PIC_STRUCT_FRAME;
    encodeConfig.isYuv444 = 0;
    encodeConfig.numB = 3;
    encodeConfig.width = TILEWIDTH;
    encodeConfig.height = TILEHEIGHT;

    encodeConfig.fOutput = fopen(encodeConfig.outputFileName, "wb");
    if (encodeConfig.fOutput == NULL)
    {
        PRINTERR("Failed to create \"%s\"\n", encodeConfig.outputFileName);
        return 1;
    }

    InitCuda(encodeConfig.deviceID);
 

    if (encodeConfig.deviceType != NV_ENC_CUDA)
        nvStatus = m_pNvHWEncoder->Initialize(m_pDevice, NV_ENC_DEVICE_TYPE_DIRECTX);
    else {
        nvStatus = m_pNvHWEncoder->Initialize(m_pDevice, NV_ENC_DEVICE_TYPE_CUDA);
	}
	if (nvStatus != NV_ENC_SUCCESS){
		printf("1.%s\n", _nvencGetErrorEnum(nvStatus));
		exit(0);
	}
	
    encodeConfig.presetGUID = m_pNvHWEncoder->GetPresetGUID(encodeConfig.encoderPreset, encodeConfig.codec);
    nvStatus = m_pNvHWEncoder->CreateEncoder(&encodeConfig);

    
    if (nvStatus != NV_ENC_SUCCESS){
        printf("Initilizing encoder 1: %s\n", _nvencGetErrorEnum(nvStatus));
        return 1;
    }
    m_uEncodeBufferCount = encodeConfig.numB + 4; // min buffers is numb + 1 + 3 pipelining

    m_uPicStruct = encodeConfig.pictureStruct;

    AllocateIOBuffers(encodeConfig.width, encodeConfig.height, encodeConfig.isYuv444);
	
	/* Weird bug when testing the if settings, even though it allocated the io buffers successfully, it test it as true
    if (nvStatus != NV_ENC_SUCCESS)
		printf("ASDFASDFASDFASDFASDFASDFKJASDF\n\n\n\n\n");
		printf("Allocate buffers 1: %s\n", _nvencGetErrorEnum(nvStatus));
        return 1;
*/
	
	
	Task *t = NULL; 

	NvEncPictureCommand reconf;
	reconf.bResolutionChangePending = true;
	reconf.bForceIDR = true;
	reconf.newWidth = TILEWIDTH;
	reconf.newHeight = TILEHEIGHT;
	
	bool first = false;
	bool firstFrame = true;
	
	AVFrame *localDummy, *temp3 = getDummyFrame(true, PXLFORMAT), *temp4;
	struct SwsContext* sws_ctx_local = getSwsContext();
	unsigned long long lStart, lEnd, lFreq;
	while(true){
	
		if(first){
			//printf("ASDFASDF\n");
			t = getTask();
			if(t == NULL) goto exit;
			if(TILES == 64){
				
				
				
			}
			reconf.qp = atoi(crf_values[t->quality].c_str());
			reconf.filename = fopen(t->filename.c_str(), "wb");
			m_pNvHWEncoder->NvEncReconfigureEncoder(&reconf);
			firstFrame = true;
		} 
		int offY = 0, offU = 0, offV = 0, countX = 0, countY = 0, offset = 0, y = 0;
		int localCounter = 0;
		int tile = tileNumber;
		if(t != NULL) tile = t->tileNumber;
		for (int frm = encodeConfig.startFrameIdx; frm <= encodeConfig.endFrameIdx; frm++)
		{
		   //numBytesRead = 0;
		   //loadframe(yuv, hInput, frm, encodeConfig.width, encodeConfig.height, numBytesRead, encodeConfig.isYuv444);

		   if(localCounter == 90){
			break;
		   }
			
			localDummy = getDummyFrame(true, AV_PIX_FMT_YUV422P);
			countX = tile % XTILES;
			countY = tile / YTILES;
			offY = (countX * TILEWIDTH) + (countY *TILEWIDTH) * SOURCE_VIDEO_HEIGHT;
			offU = offY/2;
			offV = offU;
			
		//	printf("x %d y %d offy %d u %d v %d\n", countX, countY, offY, offU, offV);
		//	printf("tileN %d xTiles %d yTiles %d enWidth %d enHeigth %d\n", tileNumb, XTILES, YTILES, encodeConfig.width, encodeConfig.height);
			temp4 = buffer[localCounter];
			if(temp4 == NULL){
				printf("AVFrame is NULL\n");
				exit(0);
			}
			
			offset = 0;
			for(y = 0; y < encodeConfig.height; y++)
			{
				memcpy(localDummy->data[0] + offset, temp4->data[0] + offY, localDummy->linesize[0]);
				offY += temp4->linesize[0];
				offset = offset + localDummy->linesize[0];
			}
			
			
			//Write the U data to file
			offset = 0;
			for(y = 0; y < encodeConfig.height; y++)
			{
				memcpy(localDummy->data[1] + offset, temp4->data[1] + offU, localDummy->linesize[1]);
				offU += temp4->linesize[1];
				offset = offset + localDummy->linesize[1];
			}

			offset = 0;
			for(y = 0; y < encodeConfig.height; y++)
			{
				memcpy(localDummy->data[2] + offset, temp4->data[2] + offV, localDummy->linesize[2]);
				offV += temp4->linesize[2];
				offset = offset + localDummy->linesize[2];
			}
		
			NvQueryPerformanceCounter(&lStart);
			sws_scale(sws_ctx_local,(uint8_t const * const *)localDummy->data, localDummy->linesize,0 ,TILEHEIGHT ,temp3->data, temp3->linesize);
			NvQueryPerformanceCounter(&lEnd);
			NvQueryPerformanceFrequency(&lFreq);
			double elapsedTime = (double)(lEnd - lStart);
			convertT1 += elapsedTime;
			
			//printf("Time it took to convert %6.2fms\n", (elapsedTime*1000.0)/lFreq);
			
			

			localCounter++;
			
			MyEncodeFrame(temp3, false, firstFrame);
			firstFrame = false;
		//	numFramesEncoded++;
			freeFrame(localDummy);
		}

		nvStatus = MyEncodeFrame(NULL, true, false);
		
		if (nvStatus != NV_ENC_SUCCESS)
		{
			bError = true;
			//first = true;
			goto exit;
		} else {
			first = true;
		}
	}
	//	currentTask = NULL;
		/*
		currentTask = getTask();
		newTask = true;
		*/
		
    
	// }

exit:

    if (encodeConfig.fOutput) {
		fclose(encodeConfig.fOutput);
		//printf("ASDFASDFASDFASDFASDFASDFKJASDF\n\n\n\n\n");
	}
	
    Deinitialize(encodeConfig.deviceType);
	freeFrame(temp3);
	
    return bError ? 1 : 0;
	
}

/**
    Initialize and allocate a memory space for storing data
*/
AVFrame *getDummyFrame(bool aTile, AVPixelFormat format){
    int ret = 0;
    AVFrame *frame = NULL;
    //int nbytes = avpicture_get_size(AV_PIX_FMT_NV12, TILEWIDTH, TILEHEIGHT);
    //uint8_t* outbuffer = (uint8_t*)av_malloc(nbytes*sizeof(uint8_t));
    frame = av_frame_alloc();


    if (!frame) {
        fprintf(stderr, "could not allocate frame\n");
        exit(1);
    }

    frame->format = format;

    if(aTile){
        frame->width = TILEWIDTH;
		if(format == AV_PIX_FMT_YUV420P && TILES == 64) frame->height = GPUHEIGHT;
        else frame->height = TILEHEIGHT;

    } else {
        frame->width = SOURCE_VIDEO_WIDTH;
        frame->height = SOURCE_VIDEO_HEIGHT;
    }

    ret = av_image_alloc(frame->data, frame->linesize, frame->width, frame->height, (AVPixelFormat)frame->format, 32);
   // printf("0 %d\n1 %d\n2 %d\n", frame->linesize[0], frame->linesize[1], frame->linesize[2]);
    if (ret < 0) {
        fprintf(stderr, "could not allocate the image\n");
        exit(1);
    }

    return frame;
}

void freeFrame(AVFrame *frame){
    av_frame_free(&frame);
}

/*Store all frames in memory*/
SwsContext *getSwsContext(){
	int height = TILEHEIGHT;
	if(TILES == 64 && PXLFORMAT == AV_PIX_FMT_YUV420P) height = GPUHEIGHT;
	struct SwsContext* sws_ctx = sws_getContext
    (
            TILEWIDTH,
            TILEHEIGHT,
            AV_PIX_FMT_YUV422P,
            TILEWIDTH,
			height,
            PXLFORMAT,
            SWS_PARAMETER,
            NULL,
            NULL,
            NULL
    );
	return sws_ctx;
}

/*Used to convert the pixelformat and resizing the resolution*/
SwsContext *initSContext(){
    struct SwsContext *sws_ctx = sws_getContext
    (
            TILEWIDTH,
            TILEHEIGHT,
            AV_PIX_FMT_YUV422P,
            TILEWIDTH,
            TILEHEIGHT,
            PXLFORMAT,
            SWS_PARAMETER,
            NULL,
            NULL,
            NULL
    );
	return sws_ctx;
}



std::vector<AVFrame*> frameBuffer;

void storeFrames(){
    AVPacket packet;
    AVFrame *frame = NULL, *temp = NULL;
  //  uint8_t *buffer = NULL;
  //  int numBytes = 0;
    int frameFinished;
    //Count the number of frames
    int counter = 0;
    //reserve 100 elements in vector
    frameBuffer.reserve(sizeof(AVFrame*)*100);

    while(av_read_frame(inFormatContext, &packet) >= 0){
        if(packet.stream_index == 0){
            frame = av_frame_alloc();
            avcodec_decode_video2(inFormatContext->streams[0]->codec, frame, &frameFinished, &packet);
            //cout << "Frame " << counter << " PictureType: " << frame->pict_type << endl;

            if(frameFinished)
            {
           
             //      printf("\rFrame %d", counter);
                  //  sws_scale(sws_ctx,(uint8_t const * const *)frame->data, frame->linesize,0 ,SOURCE_VIDEO_HEIGHT ,convert->data, convert->linesize);
                    temp = av_frame_clone(frame);
                    buffer[counter] = temp;
                    frameBuffer.push_back(temp);
                    counter++;
                 //   freeFrame(convert);
                    freeFrame(frame);
            }
            else {
                freeFrame(frame);
            }
        }
        av_free_packet(&packet);
    }

  //  printf("\n");
	//Flush the decoder, store the remaining frames
    while(true){
        av_init_packet(&packet);
        frame = av_frame_alloc();
        packet.data = NULL;
        packet.size = 0;
        avcodec_decode_video2(inFormatContext->streams[0]->codec, frame, &frameFinished, &packet);
        if(frameFinished)
            {

             //   convert = getDummyFrame(false, PXLFORMAT);
                // avpicture_fill((AVPicture *)convert, buffer, AV_PIX_FMT_YUV420P, SOURCE_VIDEO_WIDTH, SOURCE_VIDEO_HEIGHT);
            //    printf("\rFrame %d", counter);
                //cout << "frame " << counter << endl;
              //  sws_scale(sws_ctx,(uint8_t const * const *)frame->data, frame->linesize,0 ,SOURCE_VIDEO_HEIGHT ,convert->data, convert->linesize);
                temp = av_frame_clone(frame);
                buffer[counter] = temp;
                frameBuffer.push_back(temp);
                counter++;
               // freeFrame(convert);
                freeFrame(frame);

            } else {
                freeFrame(frame);
                break;
        }
        av_free_packet(&packet);
    }
  //  printf("\n");
    //Substract 1 to include index 0
    frames_total = counter;
	/*
	int check = 0;
	 while(true){
		if(buffer[check++] != NULL) printf("%d\n", buffer[check-1]->coded_picture_number);
		else {
				printf("Number %d\n", check);
				break;
		}
    }
	*/
   // exit(0);
}

AVFrame* buffer2[90];
void storeFrames2(){
	 AVPacket packet;
    AVFrame *frame = NULL, *temp = NULL;
  //  uint8_t *buffer = NULL;
  //  int numBytes = 0;
    int frameFinished;
    //Count the number of frames
    int counter = 0;
    //reserve 100 elements in vector

    while(av_read_frame(inFormatContext, &packet) >= 0){
        if(packet.stream_index == 0){
            frame = av_frame_alloc();
            avcodec_decode_video2(inFormatContext->streams[0]->codec, frame, &frameFinished, &packet);
            //cout << "Frame " << counter << " PictureType: " << frame->pict_type << endl;

            if(frameFinished)
            {
		//		printf("\rFrame %d", counter);
				buffer2[counter] = av_frame_clone(frame);
                counter++;
            }
            else {
                freeFrame(frame);
            }
        }
        av_free_packet(&packet);
    }

  //  printf("\n");
	//Flush the decoder, store the remaining frames
    while(true){
        av_init_packet(&packet);
        frame = av_frame_alloc();
        packet.data = NULL;
        packet.size = 0;
        avcodec_decode_video2(inFormatContext->streams[0]->codec, frame, &frameFinished, &packet);
        if(frameFinished)
            {
             //   printf("\rFrame %d", counter);
				buffer2[counter] = av_frame_clone(frame);
                counter++;
            } else {
                freeFrame(frame);
                break;
        }
        av_free_packet(&packet);
    }
  //  printf("\n");
    //Substract 1 to include index 0
    frames_total = counter;
   // exit(0);
	
	int check = 0;
	 while(true){
		if(buffer2[check++] != NULL) printf("%d\n", buffer2[check-1]->coded_picture_number);
		else break;
    }
}


AVFrame *getFrame(int index){
    if(index > (int)frameBuffer.size()) return NULL;
    if(frameBuffer[index] != NULL){
       // printf("Returning frame %d!!\n", index);
        return frameBuffer[index];
    } else return NULL;
}

//std::atomic<int> cnt = ATOMIC_VAR_INIT(0);
/*
std::vector<std::vector<AVFrame*>> convertedFrames;
AVFrame *tiledFrames[TILES][90];
void TileAndConvert(){
    int countX, countY, offY, offU, offV, offset, y;

    AVFrame *frame = getDummyFrame(true, AV_PIX_FMT_YUV422P);
#if defined (GPU)
    AVFrame *convert;
#endif // defined
    AVFrame *inFrame = NULL;
    for(int i = 0; i < 90;i++){
        inFrame = getFrame(i);

        for(int index = 0; index < TILES; index++){
            countX = index % XTILES;
            countY = index / YTILES;
            offY = (countX * frame->linesize[0]) + (countY * frame->linesize[0]) * inFrame->height;
            offU = offY/2;
            offV = offU;

           // printf("line1: %d\nline2: %d\nline3: %d\n", inFrame->linesize[0], inFrame->linesize[1], inFrame->linesize[2]);
           // printf("line1: %d\nline2: %d\nline3: %d\n", frame->linesize[0], frame->linesize[1], frame->linesize[2]);
            //exit(0);
            offset = 0;
            for(y = 0; y < frame->height;y++){

                memcpy(frame->data[0] + offset, inFrame->data[0] + offY, frame->linesize[0]);

                offY += inFrame->linesize[0];
                offset = offset + frame->linesize[0];
            }

            //Write the U data to file
            offset = 0;
            for(y = 0; y < frame->height;y++){
                memcpy(frame->data[1] + offset, inFrame->data[1] + offU, frame->linesize[1]);
                offU += inFrame->linesize[1];
                offset = offset + frame->linesize[1];
            }

            //Wrtie the V data to file

            offset = 0;
            for(y = 0; y < frame->height;y++){
                memcpy(frame->data[2] + offset, inFrame->data[1] + offV, frame->linesize[1]);
                offV += inFrame->linesize[1];
                offset = offset + frame->linesize[1];
            }


            frame->pts = i;
#if defined (GPU)
            convert = getDummyFrame(true, AV_PIX_FMT_NV12);
            sws_scale(sws_ctx,(uint8_t const * const *)frame->data, frame->linesize,0 ,TILEHEIGHT ,convert->data, convert->linesize);
            convert->pts = i;
            tiledFrames[index][i] = convert;
       //     printf("Tile %d Frame %d\nFrame index %d\n", index, i, (int)convert->pts);
#elif defined (CPU)
            tiledFrames[index][i] = frame;
            frame = getDummyFrame(true, AV_PIX_FMT_YUV422P);

#endif // defined
        }

    }
    freeFrame(frame);
}

AVFrame *getFramesFromTile(int tile, int index){
    if(tiledFrames != NULL && tiledFrames[tile][index] != NULL) return tiledFrames[tile][index];
    else return NULL;
}
*/

/**
    Vamsii's code, locking mechanism for threads
*/
int my_lockmgr_cb(void **lmutex, enum AVLockOp op)
 {
  if (NULL == lmutex)
   return -1;

   switch(op)
   {
   case AV_LOCK_CREATE:
   {
    *lmutex = NULL;
    std::mutex * m = new std::mutex();
    *lmutex = static_cast<void*>(m);
    break;
   }
   case AV_LOCK_OBTAIN:
   {
    std::mutex * m =  static_cast<std::mutex*>(*lmutex);
    m->lock();
    break;
   }
   case AV_LOCK_RELEASE:
   {
    std::mutex * m = static_cast<std::mutex*>(*lmutex);
    m->unlock();
    break;
   }
   case AV_LOCK_DESTROY:
   {
    std::mutex * m = static_cast<std::mutex*>(*lmutex);
    delete m;
    break;
   }
   default:
   break;
  }
return 0;
}


static std::mutex avMutex;
static bool first = true;
void initAV(){
  std::unique_lock<std::mutex> lock(avMutex);
  if(!first) return;
  first = false;
  /*register all codecs*/
  av_register_all();
 // avcodec_register(ff_nvenc_encoder)
  /*register the lock*/
  av_lockmgr_register(&my_lockmgr_cb);
}

AVFormatContext *getFormatContext(const char *filename) {
    AVFormatContext *afc = NULL;
    int ret = 0;
    AVStream *stream;
    AVCodecContext *codecContext;
    AVCodec *codec = NULL;
   // x264_param_t param;

    ret = avformat_open_input(&afc, filename, NULL, NULL);
    if(ret < 0) {
        fprintf(stderr, "Could not open input file: %s\n", filename);
        exit(ret);
    }

	/*
    ret = avformat_find_stream_info(afc, NULL);
    if(ret < 0) {
        fprintf(stderr, "could not find stream\n");
        exit(ret);
    }
*/
    /*We don't deal with several streams yet*/
    if(afc->nb_streams > 1){
        fprintf(stderr, "Too many streams, not supported yet\n");
        exit(1);
    }

    stream = afc->streams[0];
    codecContext = stream->codec;
    codec = avcodec_find_decoder(codecContext->codec_id);
    //codecContext->pix_fmt = AV_PIX_FMT_YUV420P;
	/*Settings*/
    av_opt_set(codecContext->priv_data, "preset", "very fast", 0);
    av_opt_set(codecContext->priv_data, "tune", "fastdecode", 0);
    av_opt_set(codecContext->priv_data, "profile", "high", 0);
   // av_opt_set(codecContext->priv_data, "goppattern", "3", 0);

    codecContext->max_b_frames = 3;
    codecContext->gop_size = 90;
 //   codec
   // codecContext->rc_strategy = X264_RC_CRF;

    ret = avcodec_open2(codecContext, codec, NULL);
    if(ret < 0) {
        fprintf(stderr, "Couldn't open the decoder for stream\n");
        exit(1);
    }

    /*Dump out the information of the file*/
    av_dump_format(afc, 0, filename, 0);
    return afc;
}

void initFile(char *filename){
    //if(inFormatContext != NULL)
    //Get an AVFormatContext
    inFormatContext = getFormatContext(filename);
    /*Read the Video and split it into frames*/
    storeFrames();
}


void freeStoredFrames(){
    for(int index = 0; index < (int)frameBuffer.size(); index++){
        // av_freep(&frameBuffer[index]->data[0]);
        freeFrame(frameBuffer[index]);
    }
    frameBuffer.resize(0);
    avformat_free_context(inFormatContext);
}

/*Decides everything about the structure of an output stream*/
AVCodecContext *getCodecContext(std::string crf_value){
    AVCodecContext *c = NULL;
    AVCodec *codec = NULL;

    codec = avcodec_find_encoder_by_name("libx264");

    //codec = avcodec_find_encoder_by_name("rawvideo");
    if(!codec) {
        fprintf(stderr, "Could not find codec\n");
        exit(1);
    }

    c = avcodec_alloc_context3(codec);
    if(!c) {
        fprintf(stderr, "Could not allocate video encoder\n");
        exit(1);
    }

      /**
    Preset to use:
    ultrafast - fastest encoding, tradeoff speed for space
    veryslow - best compression, tradeoff space for speed
    */
  //  if(AV_PIX_FMT_NV12 == PXLFORMAT && strcmp("nvenc", ENCODER) == 0) av_opt_set(c->priv_data, "preset", "ll", 0);
   // else 
	   //ultrafast
		av_opt_set(c->priv_data, "preset", "ultrafast", 0);
   // av_opt_set(c->priv_data, "preset", "veryfast", 0);
    //av_opt_set(c->priv_data, "goppattern", "3", 0);

       /*awesome, set CRF value*/
    av_opt_set(c->priv_data, "crf", crf_value.c_str(), 0);


    c->width = TILEWIDTH;
    c->height = TILEHEIGHT;
    c->gop_size = 90;
    c->refs = 1;
    c->keyint_min = 30;
    c->scenechange_threshold = 0;
    c->time_base.den = 30;
    c->time_base.num = 1;
    c->ticks_per_frame = 1;
	c->max_b_frames = 3;

    /*Just put the Y U V channels in the right places in an AVFrame then the encoder will take care of the rest*/
    c->pix_fmt = PXLFORMAT;
    /*Use only the amount of THREADS threads per Tile*/
    c->thread_count = THREADS;
    return c;
}

std::string addFolderName(std::string folder, char *videoName, int x, int y){
    std::string filename;

    char xCoord[5], yCoord[5];

    std::stringstream ss;
    snprintf(xCoord, 5, "%.04d", x);
    snprintf(yCoord, 5, "%.04d", y);

    ss << folder.c_str() << videoName << "." << xCoord << "_" << yCoord << ".h264";

    //ss << ".h264";
    filename = ss.str();
        //printf("%s \n", filenames[index].c_str());
    //}
    return filename;
}

void addTasksToQueue(char *file)
{
    std::string filenames[TILES][TOTAL_RES];

    char tmp[50];
    int len = strstr(file, ".h264") - file;
    strncpy(tmp, file, len);

    AVCodecContext *cContext[TILES][TOTAL_RES];
    Task *job;

    int countX = 0, countY = 0, offY = 0, offX = 0;
    std::string folder;
    for(int index = 0; index < TILES; index++)
    {
        countX = index % XTILES;
        countY = index / YTILES;

        for(int resolution = 0; resolution < TOTAL_RES; resolution++)
        {
            offY = countY * TILEHEIGHT;
            offX = countX * TILEWIDTH;

            cContext[index][resolution] = getCodecContext(crf_values[resolution]);
            if(TILES == 1 || TILES == 224) filenames[index][resolution] = addFolderName(fStruct.NoTile_crf_folders[resolution], tmp, offX, offY);
            if(TILES == 4) filenames[index][resolution] = addFolderName(fStruct.Tile2X2_crf_folders[resolution], tmp, offX, offY);
            if(TILES == 16) filenames[index][resolution] = addFolderName(fStruct.Tile4X4_crf_folders[resolution], tmp, offX, offY);
            if(TILES == 64) filenames[index][resolution] = addFolderName(fStruct.Tile8X8_crf_folders[resolution], tmp, offX, offY);

            job = new Task();
            job->cContext = cContext[index][resolution];
            job->quality = resolution;
            job->tileNumber = index;
            job->filename = filenames[index][resolution];

            jobs.push(job);
            //printf("%s\n", filenames[index][resolution].c_str());
        }
    }
  //  printf("Total of jobs %d\n", (int)jobs.size());
}

Task *getTask(){
   // cout << this_thread::get_id << " Ask for key\n";
   lockQueue.lock();
   Task *job = NULL;
   if(jobs.empty()){
	   lockQueue.unlock();
	   return NULL;
   } else {
	
   job = jobs.front();
   jobs.pop();

   lockQueue.unlock();
 
    return job;
   }
}


void startEncoding()
{
   // printf("Encoding...\n");
    std::fstream file;
    /*Parameters*/
    Task *t = NULL;
	AVCodec *localCodec = avcodec_find_encoder_by_name("libx264");
    if(localCodec == NULL){
        printf("Can't find codec\n");
        exit(1);
    }
   /*
   sharedCodec = avcodec_find_encoder_by_name("libx264");
    if(sharedCodec == NULL){
        printf("Can't find codec\n");
        exit(1);
    }
*/

    int localFrameNumber, got_output, ret = 0;
    AVFrame *frame = getDummyFrame(true, AV_PIX_FMT_YUV422P);
	//AVFrame *tFrame = getDummyFrame(true, AV_PIX_FMT_YUV420P);
	AVFrame *convert = getDummyFrame(true, PXLFORMAT);
    AVFrame *inFrame;
    AVPacket pkt;

    struct timeval start, end;
    long mtime, seconds, useconds;
	struct SwsContext* sws_ctx_local = initSContext();
    gettimeofday(&start, NULL);
	
    while(!jobs.empty())
    {
        t = getTask();

        //std::cout << std::this_thread::get_id() << " Task " << t->tileNumber << std::endl;
        if(!avcodec_is_open(t->cContext))
            if(avcodec_open2(t->cContext, localCodec, NULL) < 0)
            {
                printf("same same\n");
                exit(1);
            }

        //av_opt_set(t->cContext, "priv_data", )
		 file.open(t->filename.c_str(), std::ios::out | std::ios::binary | std::ios::trunc);
				//file.open("Test.h264", std::ios::out | std::ios::binary | std::ios::trunc);
				if (!file.is_open())
				{
					fprintf(stderr, "could not open file\n");
					exit(1);
				}
        localFrameNumber = 0;
		/**/
       // printf("Size %d\n", (int)sizeof(t->cContext->priv_data));
	  
	   
		int posX = t->tileNumber % XTILES;
		int posY = t->tileNumber / YTILES;
		int position = (posX * frame->linesize[0]) + (posY * frame->height) * SOURCE_VIDEO_WIDTH;
        while(true)
        {
            av_init_packet(&pkt);
            pkt.data = NULL; // packet data will be allocated by the encoder
            pkt.size = 0;
            fflush(stdout);
            if(localFrameNumber == 90) break;

           // inFrame = getFrame(localFrameNumber);
            inFrame = buffer[localFrameNumber];

            int offY, offU, offV, offset, y;
			/*
            if(AV_PIX_FMT_YUV422P != PXLFORMAT){
                sws_scale(sws_ctx,(uint8_t const * const *)inFrame->data, inFrame->linesize,0 ,TILEHEIGHT ,frametest->data, frametest->linesize);
                inFrame = av_frame_clone(frametest);
            }
			*/
			/*Tiling*/
			
            
			
            //offY = (countX * frame->linesize[0]) + (countY * frame->linesize[0]) * inFrame->height;
           
			offY = position;
      
			offU = offY/2;
            offV = offU;
			
			
			/**
			TODO
			Put everything on ONE LOOP
			*/
			int oY = 0;
			int oU = 0;
			int oV = 0;
			if(TILEHEIGHT != SOURCE_VIDEO_HEIGHT){
				 offset = 0;
				for(y = 0; y < frame->height; y++)
				{
					memcpy(frame->data[0] + offset, inFrame->data[0] + offY, frame->linesize[0]);
					offY += inFrame->linesize[0];
					
					offset = offset + frame->linesize[0];
				}
				//Write the U data to file
				
				offset = 0;
				for(y = 0; y < frame->height; y++)
				{
					memcpy(frame->data[1] + offset, inFrame->data[1] + offU, frame->linesize[1]);

					offU += inFrame->linesize[1];
					offset = offset + frame->linesize[1];
				}
		 

					offset = 0;
					for(y = 0; y < frame->height; y++)
					{
						memcpy(frame->data[2] + offset, inFrame->data[2] + offV, frame->linesize[2]);
						offV += inFrame->linesize[2];
						offset = offset + frame->linesize[2];
					}
					
					 frame->pts = localFrameNumber; 
	/*
				for(y = 0; y < frame->height; y++)
				{
					memcpy(frame->data[0] + oY, inFrame->data[0] + offY, frame->linesize[0]);
					offY += inFrame->linesize[0];
					
					oY = oY + frame->linesize[0];
	
					memcpy(frame->data[1] + oU, inFrame->data[1] + offU, frame->linesize[1]);
		
					offU += inFrame->linesize[1];
					oU = oU + frame->linesize[1];
			
					memcpy(frame->data[2] + oV, inFrame->data[2] + offV, frame->linesize[2]);
					offV += inFrame->linesize[2];
					oV = oV + frame->linesize[2];
				}
				frame->pts = localFrameNumber;
					 */
			} 
			/*End of tiling*/
			

			/*
			sws_scale(sws_ctx_local,(uint8_t const * const *)tFrame->data, tFrame->linesize,0 ,TILEHEIGHT ,frame->data, frame->linesize);
			
			//test
			offY = position;
      
		   offU = offY/2;
            offV = offU;

            offset = 0;
            for(y = 0; y < convert->height; y++)
            {
                memcpy(convert->data[0] + offset, tFrame->data[0] + offY, convert->linesize[0]);
                offY += tFrame->linesize[0];
                offset = offset + convert->linesize[0];
            }
            //Write the U data to file
			
            offset = 0;
            for(y = 0; y < convert->height/2; y++)
            {
                memcpy(convert->data[1] + offset, tFrame->data[1] + offU, convert->linesize[1]);
 
                offU += tFrame->linesize[1];
                offset = offset + convert->linesize[1];
            }
     

                offset = 0;
                for(y = 0; y < convert->height/2; y++)
                {
                    memcpy(convert->data[2] + offset, tFrame->data[2] + offV, convert->linesize[2]);
                    offV += tFrame->linesize[2];
                    offset = offset + convert->linesize[2];
                }
			convert->pts = localFrameNumber;
			*/
			if(PXLFORMAT != AV_PIX_FMT_YUV422P){
				
				if(TILEHEIGHT != SOURCE_VIDEO_HEIGHT){
					//printf("yeah!\n");
					sws_scale(sws_ctx_local,(uint8_t const * const *)frame->data, frame->linesize,0 ,TILEHEIGHT ,convert->data, convert->linesize);
					ret = avcodec_encode_video2(t->cContext, &pkt, convert, &got_output);
				} else {
					sws_scale(sws_ctx_local,(uint8_t const * const *)inFrame->data, inFrame->linesize,0 ,TILEHEIGHT ,convert->data, convert->linesize);
					ret = avcodec_encode_video2(t->cContext, &pkt, convert, &got_output);
				}
			} else {
				
				if(TILEHEIGHT != SOURCE_VIDEO_HEIGHT){
					ret = avcodec_encode_video2(t->cContext, &pkt, frame, &got_output);
				} else {
					ret = avcodec_encode_video2(t->cContext, &pkt, inFrame, &got_output);
				}
			}
				
            if(ret < 0)
            {
                fprintf(stderr, "Couldnt encode video\n");
                exit(1);
            }
			
            if(got_output)
            {
				/*
				 file.open(t->filename.c_str(), std::ios::out | std::ios::binary | std::ios::trunc);
				//file.open("Test.h264", std::ios::out | std::ios::binary | std::ios::trunc);
				if (!file.is_open())
				{
					fprintf(stderr, "could not open file\n");
					exit(1);
				}
				*/
                file.write((char*)pkt.data, pkt.size);
                av_free_packet(&pkt);

            }
            localFrameNumber++;
        }

        /*Flush the encoder for the remaing frames*/
        while(true)
        {
            av_init_packet(&pkt);
            pkt.data = NULL; // packet data will be allocated by the encoder
            pkt.size = 0;

            ret = avcodec_encode_video2(t->cContext, &pkt, NULL, &got_output);

            if(got_output)
            {	
		/*
				 file.open(t->filename.c_str(), std::ios::out | std::ios::binary | std::ios::trunc);
				//file.open("Test.h264", std::ios::out | std::ios::binary | std::ios::trunc);
				if (!file.is_open())
				{
					fprintf(stderr, "could not open file\n");
					exit(1);
				}*/
                file.write((char*)pkt.data, pkt.size);
				//file.close();
                av_free_packet(&pkt);
            }
            else
            {
                break;
            }
        }

        //Close the session
        avcodec_close(t->cContext);
        file.close();
    }

    /*Free the struct*/


    av_free(t->cContext);
    av_frame_free(&frame);
   // av_frame_free(&frame);

    gettimeofday(&end, NULL);
    seconds = end.tv_sec - start.tv_sec;
    useconds = end.tv_usec - start.tv_usec;
    mtime = ((seconds) * 1000 + useconds/1000.0);
   // std::cout << std::this_thread::get_id() << " Time: " << mtime << " ms" << std::endl;
    //printf("%d Time %d ms\n", std::this_thread::get_id(), mtime);
}

void startEncoding2()
{
   // printf("Encoding...\n");
    std::fstream file;
    /*Parameters*/
    Task *t = NULL;
	AVCodec *localCodec = avcodec_find_encoder_by_name("libx264");
    if(localCodec == NULL){
        printf("Can't find codec\n");
        exit(1);
    }
   /*
   sharedCodec = avcodec_find_encoder_by_name("libx264");
    if(sharedCodec == NULL){
        printf("Can't find codec\n");
        exit(1);
    }
*/

    int localFrameNumber, got_output, ret = 0;
    AVFrame *frame = getDummyFrame(true, AV_PIX_FMT_YUV422P);
	//AVFrame *tFrame = getDummyFrame(true, AV_PIX_FMT_YUV420P);
	AVFrame *convert = getDummyFrame(true, PXLFORMAT);
    AVFrame *inFrame;
    AVPacket pkt;

	struct SwsContext* sws_ctx_local = initSContext();
	
    
        t = getTask();
		if(t == NULL) return;
        if(!avcodec_is_open(t->cContext))
            if(avcodec_open2(t->cContext, localCodec, NULL) < 0)
            {
                printf("same same\n");
                exit(1);
            }

        localFrameNumber = 0;
        file.open(t->filename.c_str(), std::ios::out | std::ios::binary | std::ios::trunc);
        if (!file.is_open())
        {
            fprintf(stderr, "could not open file\n");
            exit(1);
        }

	   
		int posX = t->tileNumber % XTILES;
		int posY = t->tileNumber / YTILES;
		int position = (posX * frame->linesize[0]) + (posY * frame->height) * SOURCE_VIDEO_WIDTH;
        while(true)
        {
            av_init_packet(&pkt);
            pkt.data = NULL; // packet data will be allocated by the encoder
            pkt.size = 0;
            fflush(stdout);
            if(localFrameNumber == 90) break;

            inFrame = buffer[localFrameNumber];

            int offY, offU, offV, offset, y;

			offY = position;
      
		   offU = offY/2;
            offV = offU;

			if(TILEHEIGHT != SOURCE_VIDEO_HEIGHT){
				offset = 0;
				for(y = 0; y < frame->height; y++)
				{
					memcpy(frame->data[0] + offset, inFrame->data[0] + offY, frame->linesize[0]);
					offY += inFrame->linesize[0];
					offset = offset + frame->linesize[0];
				}
				//Write the U data to file
				offset = 0;
				for(y = 0; y < frame->height; y++)
				{
					memcpy(frame->data[1] + offset, inFrame->data[1] + offU, frame->linesize[1]);
					offU += inFrame->linesize[1];
					offset = offset + frame->linesize[1];
				}
		 

					offset = 0;
					for(y = 0; y < frame->height; y++)
					{
						memcpy(frame->data[2] + offset, inFrame->data[2] + offV, frame->linesize[2]);
						offV += inFrame->linesize[2];
						offset = offset + frame->linesize[2];
					}
					
					 frame->pts = localFrameNumber;
			} 
			/*End of tiling*/
			
			if(PXLFORMAT != AV_PIX_FMT_YUV422P){
				
				if(TILEHEIGHT != SOURCE_VIDEO_HEIGHT){
					sws_scale(sws_ctx_local,(uint8_t const * const *)frame->data, frame->linesize,0 ,TILEHEIGHT ,convert->data, convert->linesize);
					ret = avcodec_encode_video2(t->cContext, &pkt, convert, &got_output);
				} else {
					sws_scale(sws_ctx_local,(uint8_t const * const *)inFrame->data, inFrame->linesize,0 ,TILEHEIGHT ,convert->data, convert->linesize);
					ret = avcodec_encode_video2(t->cContext, &pkt, convert, &got_output);
				}
			} else {
				
				if(TILEHEIGHT != SOURCE_VIDEO_HEIGHT){
					ret = avcodec_encode_video2(t->cContext, &pkt, frame, &got_output);
				} else {
					ret = avcodec_encode_video2(t->cContext, &pkt, inFrame, &got_output);
				}
			}
				
            if(ret < 0)
            {
                fprintf(stderr, "Couldnt encode video\n");
                exit(1);
            }
			
            if(got_output)
            {
                file.write((char*)pkt.data, pkt.size);
                av_free_packet(&pkt);

            }
            localFrameNumber++;
        }

        /*Flush the encoder for the remaing frames*/
        while(true)
        {
            av_init_packet(&pkt);
            pkt.data = NULL; // packet data will be allocated by the encoder
            pkt.size = 0;
            ret = avcodec_encode_video2(t->cContext, &pkt, NULL, &got_output);

            if(got_output)
            {
                file.write((char*)pkt.data, pkt.size);
                av_free_packet(&pkt);
            }
            else
            {
                break;
            }
        }
        //Close the session
        avcodec_close(t->cContext);
        file.close();
    
    /*Free the struct*/
    av_free(t->cContext);
    av_frame_free(&frame);
  //  av_frame_free(&frame);
	}


/*Create a subfolder in SOURCE_FOLDER*/
std::string makeFolder(std::string name, std::string source){
    std::stringstream ss;
    ss << source.c_str() << name.c_str() << "/";
    mkdir(ss.str().c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    return ss.str();
}

/**
    Create the whole folder structure which will store the encoded tiles
*/
void makeFolderStructure(){
    /*If the folder doesn't exist, create the folders, else return*/
    mkdir(SOURCE_FOLDER, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    fStruct.tiles2x2 = makeFolder(TILES2X2, SOURCE_FOLDER);
    fStruct.tiles4x4 = makeFolder(TILES4X4, SOURCE_FOLDER);
    fStruct.tiles8x8 = makeFolder(TILES8X8, SOURCE_FOLDER);
    fStruct.notile = makeFolder("NoTile", SOURCE_FOLDER);

    for(int i = 0; i < TOTAL_RES; i++){
        fStruct.Tile2X2_crf_folders[i] = makeFolder("out"+crf_values[i], fStruct.tiles2x2);
        fStruct.Tile4X4_crf_folders[i] = makeFolder("out"+crf_values[i], fStruct.tiles4x4);
        fStruct.Tile8X8_crf_folders[i] = makeFolder("out"+crf_values[i], fStruct.tiles8x8);
        fStruct.NoTile_crf_folders[i] = makeFolder("out"+crf_values[i], fStruct.notile);
    }
}

void startThreading(){
#if defined (GPU)
		printf("GPU\n");
        for(int i = 0; i < 1; i++){
            pool[i] = std::thread(&startEncoding);
        }

        startEncoding();

        for(int i = 0; i < 1; i++){
            pool[i].join();
        }
#elif defined (CPU)

		#if defined (PARALLEL)
		printf("PARALLEL\n");
		//Configure it manuel to use parallel on tiles or on tiles * total resolution
		while(!jobs.empty()){
		for(int i = 0; i < TOTAL_THREADS; i++){
            parallel[i] = std::thread(&startEncoding2);
        }
		
        //startEncoding2();

        for(int i = 0; i < TOTAL_THREADS; i++){
            parallel[i].join();
        }
		}
		#elif defined (THREADING)
		//printf("THREADPOOL\n");
		for(int i = 0; i < TOTAL_THREADS-1; i++){
            pool[i] = std::thread(&startEncoding);
        }

        startEncoding();

        for(int i = 0; i < TOTAL_THREADS-1; i++){
            pool[i].join();
        }
		
		#endif // defined
#endif // defined

}

void startEncodingGPU()
{





}

int gpuParallel()
{
	CNvEncoder encoder;
	Task *t = NULL;
	while(true){
		t = getTask();
		if(t == NULL) break;
		 std::string str2[] = {"./Encode", "-i", "out420.yuv", "-o", t->filename, "-size", "2048", "840", "qp", crf_values[t->quality],  "-rcmode" , "0" , "-goplength", "90", "-numB", "3"};
		 encoder.EncodeMain(16, (char**)str2, t->tileNumber);
		 break;
	}	
	return 1;
}

int gpuParallel2()
{
	CNvEncoder encoder;
	 unsigned long long lStart, lEnd, lFreq;
	Task *t = getTask();
	//while(t != NULL){
		
	//NvQueryPerformanceCounter(&lStart);
	encoder.MyEncodeMain((char*) t->filename.c_str(), t->quality, t->tileNumber, false);
//	NvQueryPerformanceFrequency(&lFreq);
//	NvQueryPerformanceCounter(&lEnd);
//	printf("Total Time to Convert from 422 to 420 %6.2fms\n", (encoder.convertT1*1000.0)/lFreq);
	//printf("Total Time to Convert from 420 to NV12 %6.2fms\n", (encoder.convertT2*1000.0)/lFreq);
//	double elapsedTime = (double)(lEnd - lStart);
//	printf("Total Time %6.2fms\n", (elapsedTime*1000.0)/lFreq);
	//encoder.MyEncodeMain("Olalal", 21, 0, false);
	//t = getTask();

//	}
//	encoder.Deinitialize(0);
	return 1;
}



const unsigned char CLIENT_KEY[] = { 230,127,93,88,29,83,108,73,133,106,93,230,163,146,36,165 };
void initializeEncoder(){
	void *m_encoder;
	 int fpsNum = 30 * 1000;
     int fpsDen = 1000;

			//Set up encoding session
            NV_ENC_OPEN_ENCODE_SESSION_EX_PARAMS sessionParams = {0};
            memset(&sessionParams, 0, sizeof(NV_ENC_OPEN_ENCODE_SESSION_EX_PARAMS));
            SET_VER(sessionParams, NV_ENC_OPEN_ENCODE_SESSION_EX_PARAMS);
            sessionParams.apiVersion = NVENCAPI_VERSION;
            sessionParams.apiVersion = NVENCAPI_VERSION;
            sessionParams.device = reinterpret_cast<void *>(m_cudaContext);
            sessionParams.deviceType = NV_ENC_DEVICE_TYPE_CUDA;
			sessionParams.clientKeyPtr = (GUID *) CLIENT_KEY;
			NVENCSTATUS nvStatus = NV_ENC_SUCCESS;
            nvStatus = nvencApi->nvEncOpenEncodeSessionEx(&sessionParams, &m_encoder);
			printf("%s\n", _nvencGetErrorEnum(nvStatus));
}

void test()
{
	printf("here1\n");
	Inittest();
	printf("here2\n");
	initializeEncoder();
	printf("here3\n");
	initializeEncoder();
	initializeEncoder();
	initializeEncoder();
	initializeEncoder();
	exit(0);

}

int main(int argc, char *argv[])
{	
	SigSevHandler::enableSignalHandler(argv[0]);
//	test();
    //std::string str[] = {"./Encode", "-i", "out420.yuv", "-o", "test2.h264", "-size", "4096", "1680", "-qp", "21",  "-rcmode" , "0" , "-goplength", "90", "-numB", "3"};
  //  CNvEncoder encoder;
  //  encoder.EncodeMain(16, (char**)str);
   // exit(0);
   /*
   THREADS = atoi(argv[4]);

	if(atoi(argv[3]) == 1){
		TILEWIDTH = 4096;
		TILEHEIGHT = 1680;
		XTILES = 1;
		YTILES = 1;
		TILES = 1;
	} else if(atoi(argv[3]) == 2){
		TILEWIDTH = 2048;
		TILEHEIGHT = 840;
		XTILES = 2;
		YTILES = 2;
		TILES = 4;
	} else if(atoi(argv[3]) == 3){
		TILEWIDTH = 1024;
		TILEHEIGHT = 420;
		XTILES = 4;
		YTILES = 4;
		TILES = 16;  
	} else {
		TILEWIDTH = 512;
		TILEHEIGHT = 210;
		XTILES = 8;
		YTILES = 8;
		TILES = 64;	  
	}
*/

  unsigned long long lStart, lEnd, lFreq;
   // initSContext();
    initAV();
    initFile(argv[1]);

#if defined (GPU)
	

  //  std::string str[] = {"./Encode", "-i", "out420.yuv", "-o", "test5.h264", "-size", "4096", "1680", "-qp", "21",  "-rcmode" , "0" , "-goplength", "90", "-numB", "3"};
    //CNvEncoder encoder;
	/*
	CNvEncoder encoder2;
	CNvEncoder encoderarray[4];
	 Task *t = NULL;
	// t = getTask();
	*/
	 makeFolderStructure();
    addTasksToQueue(argv[1]);


	
	/**
	  int tileNumber;
    int quality;
    //int frameNum;
    int x, y;
    std::string filename;
	*/
	
	
	int a = 5;
	NvQueryPerformanceCounter(&lStart);
	
//	std::thread t1(&gpuParallel);
//	std::thread t2(&gpuParallel);
	//std::thread t3(&gpuParallel);
//	gpuParallel();
//	t1.join();
//	t2.join();
//	t3.join();
	//CNvEncoder encoder;
//	encoder.MyEncodeMain();
	Inittest();
	//printf("GPU ");
	int toG = GPU_THREADS;
	
	
	std::thread GPUpool[toG];
	 for(int i = 0; i < toG; i++){
            GPUpool[i] = std::thread(&gpuParallel2);
      }
		

	 for(int i = 0; i < toG; i++){
        GPUpool[i].join();
    }
	
		NvQueryPerformanceCounter(&lEnd);
		NvQueryPerformanceFrequency(&lFreq);
		double elapsedTime = (double)(lEnd - lStart);
		int numFramesEncoded = TILES*TOTAL_RES*90;
		//printf("Encoded %d frames in %6.2fms\n", numFramesEncoded, (elapsedTime*1000.0)/lFreq);
	//	printf("Avergage Encode Time : %6.2fms\n", ((elapsedTime*1000.0)/numFramesEncoded)/lFreq);
		printf("%6.2f\n", (elapsedTime*1000.0)/lFreq);
	//printf("%6.2f\n", ((elapsedTime*1000.0)/numFramesEncoded)/lFreq);
	CUresult cuResult = CUDA_SUCCESS;
            cuResult = cuCtxDestroy((CUcontext)m_cudaContext);
            if (cuResult != CUDA_SUCCESS)
                PRINTERR("cuCtxDestroy error:0x%x\n", cuResult);
    //std:thread lo encoder.EncodeMain(16, (char**)str);
	
	//lo.join();
    return 0;
   // TileAndConvert();
#elif defined (CPU)
   // NvencContext n;

	//printf("CPU ");

    makeFolderStructure();
    addTasksToQueue(argv[1]);
	NvQueryPerformanceCounter(&lStart);
    if(TOTAL_THREADS < 2) startEncoding();
    else startThreading();
	NvQueryPerformanceCounter(&lEnd);
	NvQueryPerformanceFrequency(&lFreq);
	double elapsedTime = (double)(lEnd - lStart);
	int numFramesEncoded = TILES*TOTAL_RES*90;

	//printf("Encoded %d frames in %6.2fms\n", numFramesEncoded, (elapsedTime*1000.0)/lFreq);
	//printf("Avergage Encode Time : %6.2fms\n", ((elapsedTime*1000.0)/numFramesEncoded)/lFreq);
	printf("%6.2f \n", (elapsedTime*1000.0)/lFreq);
//	printf("%6.2f\n", ((elapsedTime*1000.0)/numFramesEncoded)/lFreq);
    freeStoredFrames();
	
#elif defined (CPUGPU)
	
	printf("GPU AND CPU ");
	 makeFolderStructure();
    addTasksToQueue(argv[1]);
	NvQueryPerformanceCounter(&lStart);
	int toG = GPU_THREADS;
	std::thread GPUpool[toG];
	for(int i = 0; i < toG; i++){
           GPUpool[i] = std::thread(&gpuParallel2);
    }
	
	for(int i = 0; i < TOTAL_THREADS-1; i++){
            pool[i] = std::thread(&startEncoding);
    }

    startEncoding();
	
    for(int i = 0; i < TOTAL_THREADS-1; i++){
         pool[i].join();
    }
	
	 for(int i = 0; i < toG; i++){
        GPUpool[i].join();
    }
	NvQueryPerformanceCounter(&lEnd);
		NvQueryPerformanceFrequency(&lFreq);
		double elapsedTime = (double)(lEnd - lStart);
		int numFramesEncoded = TILES*TOTAL_RES*90;
		printf("Encoded %d frames in %6.2fms\n", numFramesEncoded, (elapsedTime*1000.0)/lFreq);
		//printf("Avergage Encode Time : %6.2fms\n", ((elapsedTime*1000.0)/numFramesEncoded)/lFreq);
		freeStoredFrames();
	#endif // defined
   return 0;
}
